# GitLab Solutions for Open Source program: filled application form content

## First Name

```
ZDA
```

## Last Name

```
Admin
```

## Email address

local part: `zda`\
domain: `mailbox.org`

## Country

```
Switzerland
```

## Open source organization or project name:

```
zdaarau
```

## Project description

```
rdb provides access to the Referendum Database (RDB) from R. This database aims to record all direct democratic votes worldwide and is operated by the Centre for Democracy Studies Aarau (ZDA) at the University of Zurich, Switzerland.
```

## Publicly visible project on GitLab

```
https://gitlab.com/zdaarau/rpkgs/rdb
```

## OSI approved license

```
https://gitlab.com/zdaarau/rpkgs/rdb/-/blob/master/LICENSE.md
```

## Additional details

```
We are a research institute under public law that conducts political, educational and legal research and does not pursue any commercial activities. See our website: https://www.zdaarau.ch/en/

We use GitLab to develop OSS that helps doing our research.
```

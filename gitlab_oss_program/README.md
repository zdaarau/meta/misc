# GitLab for Open Source program

Since 2023, our [`zdaarau` GitLab organization](https://gitlab.com/zdaarau/) benefits from a [GitLab Ultimate](https://about.gitlab.com/pricing/ultimate/)
license thanks to the [*GitLab Solutions for Open Source* program](https://about.gitlab.com/solutions/open-source/). This license must be renewed annually by
filling the [corresponding application form](https://about.gitlab.com/solutions/open-source/join/#open-source-program-application) and submitting three specific
screenshots. After the submission has been approved (which usually takes a few days), we'll receive an e-mail with a license code and instructions on how to use
it. Licenses are managed via GitLab's [customer portal](https://customers.gitlab.com/). Our customer portal account is linked to our GitLab user account
[`zdadmin`](https://gitlab.com/zdadmin), so we must use this account to log in to the customer portal.

To facilitate the annual license renewal process, we store the [content filled in the application form](application_form.md) as well as the
[screenshots](screenshots) in this repository.

# ZDA issue tracker

Sole purpose of this project is to collect files and issues around work of the [Centre for Democracy Studies Aarau (ZDA)](https://www.zdaarau.ch/) which is not
directly related to specific code and does not already have a [dedicated meta issue tracker](https://gitlab.com/zdaarau/meta).

The issues are found [**here**](https://gitlab.com/zdaarau/meta/misc/-/issues).

## License

Code and configuration in this repository is licensed under [`AGPL-3.0-or-later`](https://spdx.org/licenses/AGPL-3.0-or-later.html). See
[`LICENSE.md`](LICENSE.md).

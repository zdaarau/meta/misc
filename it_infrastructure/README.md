# ZDA IT infrastructure

In the following, the most important third-party services we use for our IT infrastructure across multiple projects are outlined.

## Credentials

An encrypted backup of all credentials for the below services is stored in [`credentials.md.age`](credentials.md.age).

The file was created using [*age*](https://github.com/FiloSottile/age#readme)[^1]:

``` sh
age --encrypt --passphrase --output credentials.md.age credentials.md
```

It can be decrypted via (will prompt for the passphrase):

``` sh
age --decrypt --output credentials.md credentials.md.age
```

[^1]: *age* is a simple, modern and secure file encryption tool, format, and Go library. It is available for all major platforms, see the [installation
    instructions](https://github.com/FiloSottile/age#installation).

## Fly.io

We rely on [fly.io](https://fly.io/) to host our own [web applications](https://gitlab.com/zdaarau/webapps/).

Fly's administration interface can be accessed [here](https://fly.io/dashboard).

## Gandi.net

All the domains we can't register on Porkbun (which is considerably cheaper and technically more appealing) are registered via [gandi.net](https://gandi.net/).
Auto-renewal is currently enabled for all relevant domains.

Gandi's administration interface can be accessed [here](https://admin.gandi.net/).

## GitLab.com

We use GitLab.com as our main collaboration platform and Git forge. We have a dedicated GitLab account [`zdadmin`](https://gitlab.com/zdadmin) for
administration purposes only that is owner of our [`zdaarau` GitLab organization](https://gitlab.com/zdaarau/). The latter benefits from a [GitLab
Ultimate](https://about.gitlab.com/pricing/ultimate/) license thanks to the [*GitLab Solutions for Open Source*
program](https://about.gitlab.com/solutions/open-source/). This license must be renewed yearly, see the[`../gitlab_oss_program/`](../gitlab_oss_program) folder
for details.

## Mailbox.org

We use a fully managed e-mail solution by [mailbox.org](https://mailbox.org/), for which we configured our domains like `digipartindex.ch`, `fokus.ag`,
`rdb.vote` etc. as [custom domains](https://kb.mailbox.org/en/private/custom-domains) having each a [catch-all
alias](https://kb.mailbox.org/en/private/e-mail-article/using-catch-all-alias-with-own-domain) plus individual aliases as needed.

The administration and webmail interface can be accessed [here](https://login.mailbox.org/).

## Netlify.com

We use [netlify.com](https://www.netlify.com/) to host our [static sites](https://gitlab.com/zdaarau/websites/). And we use Netlify's name servers for all of
our domains, so DNS entries have to be managed via Netlify.

Netlify's administration interface can be accessed [here](https://app.netlify.com/).

## Porkbun.com

We use [porkbun.com](https://porkbun.com/) as our main domain registrar. Auto-renewal is currently enabled for all relevant domains.

Porkbun's administration interface can be accessed [here](https://porkbun.com/account).

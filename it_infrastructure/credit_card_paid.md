# Service provider accounts that require credit card payments

- [AWS](https://console.aws.amazon.com/) (2FA is managed by PITSolutions since 2024-11)
- [Backblaze.com](https://secure.backblaze.com/user_signin.htm)
- [Cloudflare.com](https://dash.cloudflare.com)
- [Fly.io](https://fly.io/dashboard)
- [Gandi.net](https://admin.gandi.net/)
- [Mailbox.org](https://login.mailbox.org/)
- [Neon.tech](https://console.neon.tech/)
- [Porkbun.com](https://porkbun.com/account)
